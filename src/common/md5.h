// Copyright 2007 Google Inc. All Rights Reserved.
// Author: liuli@google.com (Liu Li)
#ifndef COMMON_MD5_H__
#define COMMON_MD5_H__

#include <stdint.h>

#if __cplusplus
namespace google_breakpad {
#endif


#if defined(__APPLE__) || defined(__linux__)
typedef uint32_t u32;
typedef uint8_t u8;
#endif

// Use uniform definition for MD5Context. With 64-bit builds, unsigned long is
// not always the same as u32.
#if 0 // defined(__APPLE__) || defined(__linux__)

struct MD5Context {
  unsigned long buf[4];
  unsigned long bits[2];
  unsigned char in[64];
};

#else
struct MD5Context {
  u32 buf[4];
  u32 bits[2];
  u8  in[64];
};
#endif 



void MD5Init(struct MD5Context *ctx);

void MD5Update(struct MD5Context *ctx, unsigned char const *buf, size_t len);

void MD5Final(unsigned char digest[16], struct MD5Context *ctx);

#if __cplusplus
}  // namespace google_breakpad
#endif

#endif  // COMMON_MD5_H__
